<?php
namespace Apps;

class Profile {
    public $id = "";
    public $name = "";
    public $username = "";
    public $email = "";
    public $photo = "";
    public $dob = "";
    public $phone = "";
    public $status = "";
    public $password= "";
    
    public function __construct($name='robiul islam') {
        $this->name = $name;
        $this->status ='Pending';
    }
    
}
