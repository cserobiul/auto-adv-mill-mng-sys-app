# Automated Advanced Mill Management System Application [AAMMSA]

[![Facebook](https://fb.com/cserobiul)](https://fb.com/cserobiul)

Its a open source project that developed only for bachelor mess!!

## Official Documentation

Documentation for this project can be found on the [Lopa IT website](http://it.lopagroup.org).



## License

This Application is open-sourced software licensed under the [AM IT license](http://it.lopagroup.org).
